<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'v1'], function () {
    Route::post('authenticate', 'Auth\LoginController@authenticate');
});

Route::group(['prefix' => 'v1','middleware' => [ 'jwt.auth' ] ], function () {
    Route::post('profile', 'ProfileController@profile');
    Route::post('refreshToken', 'ProfileController@refreshToken');
});