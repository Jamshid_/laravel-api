<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Tymon\JWTAuth\Facades\JWTAuth;

class ProfileController extends Controller
{

    public function profile(){
       $user = JWTAuth::parseToken()->toUser();
       if(!$user) {
           return response()->json(['error' => 'User not fount'], 404);
       }
       return response()->json(['success' => true, 'profile' => $user], 200);
    }

    public function refreshToken() {

        $token = JWTAuth::getToken();

        if (! $token) {
            return response()->json(['error' => 'Token is invalid']);
        }

        try{
            $refreshedToken = JWTAuth::refresh($token);
        } catch (JWTException $e) {
            return response()->json(compact('Something went wrong'));
        }

        return response()->json(['refreshedToken' => $refreshedToken ],200);

    }
}
